# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django import forms
from core.models import Slideshow, AboutUs, Employee, Service, ServiceText, ContactUs, ArticleCategory, Article, \
	SocialNetworks, LinkCategory, Link, ProfileImage, JobOffer, Gdpr, HomeBlocks


class SlideshowForm(forms.ModelForm):
	class Meta:
		model = Slideshow
		exclude = (
			'slideshow_title',
			'slideshow_body',
			'slideshow_link',
			'slideshow_org_image',
			'slideshow_file',
			'slideshow_thumb',
			'slideshow_uuid',
		)


class AboutUsForm(forms.ModelForm):
	class Meta:
		model = AboutUs
		exclude = (
			'aboutus_body',
			'aboutus_image',
		)


class EmployeeForm(forms.ModelForm):
	class Meta:
		model = Employee
		exclude = (
			'employee_full_name',
			'employee_job',
			'employee_description',
			'employee_email',
			'employee_facebook',
			'employee_phone',
			'employee_slug',
			'employee_twitter',
			'employee_image',
		)


class ServiceTextForm(forms.ModelForm):
	class Meta:
		model = ServiceText
		exclude = (
			'service_text',
		)


class ServiceForm(forms.ModelForm):
	class Meta:
		model = Service
		exclude = (
			'service_name',
			'service_icon',
			'service_description',
		)


class ContactUsForm(forms.ModelForm):
	class Meta:
		model = ContactUs
		exclude = (
			'contact_us_name',
			'contact_us_address',
			'map_address',
		)


class ArticleCategoryForm(forms.ModelForm):
	class Meta:
		model = ArticleCategory
		exclude = (
			'article_category_title',
			'article_category_alias',
			'article_category_date',
			'article_category_level',
			'article_category_published',
			'article_category_user',
		)


class ArticleForm(forms.ModelForm):
	class Meta:
		model = Article
		exclude = (
			'article_title',
			'article_alias',
			'article_body',
			'article_category',
			'article_edited_date',
			'article_image',
			'article_metakey',
			'article_published',
		)


class SocialForm(forms.ModelForm):
	class Meta:
		model = SocialNetworks
		exclude = (
			'social_title',
			'social_icon',
			'social_link',
		)
		
		
class LinkCategoryForm(forms.ModelForm):
	class Meta:
		model = LinkCategory
		exclude = (
			'link_category_name',
			'link_category_alias',
		)


class LinkForm(forms.ModelForm):
	class Meta:
		model = Link
		exclude = (
			'link_category',
			'link_image',
			'link_name',
			'link_url',
		)
		
		
class ProfileImageForm(forms.ModelForm):
	class Meta:
		model = ProfileImage
		exclude = (
			'profile_image',
		)
		
	
class JobOffersForm(forms.ModelForm):
	class Meta:
		model = JobOffer
		exclude = (
			'job_offer_title',
			'job_offer_description',
			'job_offer_created_date',
			'job_offer_expiring_date',
			'job_offer_published'
		)
		
		
class GdprForm(forms.ModelForm):
	class Meta:
		model = Gdpr
		exclude = (
			'gdpr_link',
		)
		
		
class HomeBlocksForm(forms.ModelForm):
	class Meta:
		model = HomeBlocks
		exclude = (
			'home_block_link',
			'home_block_image',
			'home_block_title',
		)


class LoginForm(forms.Form):
	username = forms.CharField()
	password = forms.CharField(widget=forms.PasswordInput)
