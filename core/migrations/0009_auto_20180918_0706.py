# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-09-18 07:06
from __future__ import unicode_literals

import core.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_auto_20180918_0703'),
    ]

    operations = [
        migrations.AlterField(
            model_name='slideshow',
            name='slideshow_file',
            field=models.ImageField(blank=True, upload_to=core.models.get_upload_path_cropped),
        ),
        migrations.AlterField(
            model_name='slideshow',
            name='slideshow_org_image',
            field=models.ImageField(upload_to=core.models.get_upload_path),
        ),
    ]
