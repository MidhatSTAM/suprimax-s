# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-09-22 14:04
from __future__ import unicode_literals

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0013_auto_20180922_1331'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aboutus',
            name='aboutus_image',
            field=models.ImageField(blank=True, null=True, upload_to='static/img/aboutus/'),
        ),
        migrations.AlterField(
            model_name='slideshow',
            name='slideshow_uuid',
            field=models.CharField(default=uuid.UUID('c4b3aa63-94c7-41e6-8d1e-45e0322123d0'), max_length=50),
        ),
    ]
