# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-09-22 14:41
from __future__ import unicode_literals

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0021_auto_20180922_1440'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aboutus',
            name='aboutus_image',
            field=models.ImageField(upload_to='img/aboutus/'),
        ),
        migrations.AlterField(
            model_name='slideshow',
            name='slideshow_uuid',
            field=models.CharField(default=uuid.UUID('dde2fc6c-e5e6-4167-9a0a-c8b420c01188'), max_length=50),
        ),
    ]
