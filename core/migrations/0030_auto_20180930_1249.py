# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-09-30 12:49
from __future__ import unicode_literals

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0029_auto_20180923_1839'),
    ]

    operations = [
        migrations.CreateModel(
            name='ServiceText',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('service_text', models.TextField()),
            ],
            options={
                'db_table': 'servicetext',
                'verbose_name': 'Service Main Content',
                'verbose_name_plural': 'Service Main Contents',
            },
        ),
        migrations.AlterField(
            model_name='slideshow',
            name='slideshow_uuid',
            field=models.CharField(default=uuid.UUID('fe6810ba-538a-4b9f-b07a-30f1175f56eb'), max_length=50),
        ),
    ]
