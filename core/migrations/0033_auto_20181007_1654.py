# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-10-07 16:54
from __future__ import unicode_literals

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0032_auto_20181007_1649'),
    ]

    operations = [
        migrations.AddField(
            model_name='contactus',
            name='map_address',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='slideshow',
            name='slideshow_uuid',
            field=models.CharField(default=uuid.UUID('c36287e5-f755-41a0-85cf-971c8ebaf659'), max_length=50),
        ),
    ]
