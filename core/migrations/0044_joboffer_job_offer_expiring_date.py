# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-11-17 13:54
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0043_joboffer_job_offer_created_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='joboffer',
            name='job_offer_expiring_date',
            field=models.DateTimeField(default='2018-11-17 16:54:02.405485'),
            preserve_default=False,
        ),
    ]
