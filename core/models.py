# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from django.db import models
from django.contrib.auth.models import User
from django.utils.text import slugify
from datetime import date
import datetime

import uuid


# Create your models here.


def get_image_path(filename):
	path = ''.join([date.today().strftime('content/article//%Y/%m/'), slugify(filename) + str('.jpg')])
	return path


class ArticleCategory(models.Model):
	class Meta():
		db_table = "article_category"
		verbose_name = "Article category"
		verbose_name_plural = "Article categories"
	
	article_category_title = models.CharField(
		verbose_name="Menu category name",
		max_length=200
	)
	article_category_alias = models.CharField(
		verbose_name="Menu category alias",
		max_length=200
	)
	article_category_level = models.IntegerField(
		default=1,
		blank=False,
	)
	article_category_published = models.BooleanField(
		"ArticleCategory",
		default=1,
		blank=False,
	)
	article_category_date = models.DateTimeField(
		auto_now_add=True,
		blank=False,
	)
	
	def __unicode__(self):
		return u'%s' % self.article_category_title


class Article(models.Model):
	class Meta():
		db_table = "artice"
		verbose_name = "Article"
		verbose_name_plural = "Articles"
		ordering = ['-article_published', 'id']
	
	article_title = models.CharField(
		max_length=100,
		null=False,
		blank=False
	)
	article_alias = models.CharField(
		verbose_name="Article alias",
		max_length=250
	)
	article_category = models.ForeignKey(
		"ArticleCategory",
		related_name="article_category_key",
		verbose_name="Category to which belongs article",
		null=False,
		blank=False,
		default='1',
		on_delete=models.CASCADE,
	)
	article_body = models.TextField(
		null=False,
		blank=False
	)
	article_metakey = models.CharField(
		verbose_name="Article Meta Keys",
		max_length=2000,
		blank=True,
		null=True,
	)
	article_published = models.BooleanField(
		"ArticleCategory",
		default=1,
		blank=True,
		null=False,
	)
	article_edited_date = models.DateTimeField(
		verbose_name="Article modified date",
		auto_now_add=True,
		blank=False,
	)
	
	article_image = models.ImageField(
		upload_to="static/img/articles/",
		null=True,
		blank=True,
		verbose_name="Article main image")
	
	def __unicode__(self):
		return u'%s' % self.article_title + str(" (") + self.article_category.article_category_title + str(")")


class Employee(models.Model):
	class Meta():
		db_table = "employee"
		verbose_name = "Employee"
		verbose_name_plural = "Employees"
	
	employee_full_name = models.CharField(
		max_length=256,
		null=False,
		blank=False
	)
	employee_job = models.CharField(
		max_length=100,
		blank=True,
		null=True
	)
	employee_facebook = models.URLField(
		blank=True,
		null=True
	)
	employee_twitter = models.URLField(
		blank=True,
		null=True
	)
	employee_slug = models.CharField(
		blank=True,
		max_length=100
	)
	employee_description = models.TextField(
		blank=True,
		default=''
	)
	employee_email = models.EmailField(
		blank=True,
		null=True
	)
	
	employee_phone = models.CharField(
		max_length=15,
		null=True,
		blank=True
	)
	
	employee_image = models.ImageField(
		upload_to="static/img/employee/",
		blank=True,
		null=True
	)
	
	def save(self, *args, **kwargs):
		if not self.employee_slug:
			self.employee_slug = slugify(self.employee_full_name)
		super(Employee, self).save(*args, **kwargs)
	
	def __str__(self):
		return self.employee_full_name


class JobOffer(models.Model):
	class Meta():
		db_table = "joboffer"
		verbose_name = "Job Offer"
		verbose_name_plural = "Job Offers"
	
	job_offer_title = models.CharField(
		max_length=255,
		blank=False,
		null=False
	)
	
	job_offer_description = models.TextField(
		blank=False,
		null=False
	)
	
	job_offer_created_date = models.DateTimeField(
		auto_now=True,
		blank=False,
		null=False
	)
	
	job_offer_expiring_date = models.DateTimeField(
		blank=False,
		null=False
	)
	
	job_offer_published = models.BooleanField(
		default=1,
		blank=False,
		null=False
	)
	

class AboutUs(models.Model):
	class Meta():
		db_table = "aboutus"
		verbose_name = "About Us"
		verbose_name_plural = "About Us"
	
	aboutus_body = models.TextField(
		blank=False,
		null=False
	)
	
	aboutus_image = models.ImageField(
		upload_to="static/img/aboutus/",
		blank=True,
		null=True
	)


class LinkCategory(models.Model):
	class Meta():
		db_table = "link_category"
		verbose_name = "Link category"
		verbose_name_plural = "Link categories"
	
	link_category_name = models.CharField(
		max_length=255,
		blank=False,
		null=False
	)
	
	link_category_alias = models.CharField(
		max_length=255,
		blank=False,
		null=False
	)


class Link(models.Model):
	class Meta():
		db_table = "link"
		verbose_name = "Link"
		verbose_name_plural = "Links"
	
	link_url = models.URLField(
		blank=False,
		null=False
	)
	
	link_name = models.CharField(
		max_length=255,
		blank=False,
		null=False
	)
	
	link_image = models.ImageField(
		upload_to="static/img/link/",
		blank=True,
		null=True
	)
	
	link_category = models.ForeignKey(
		LinkCategory,
		related_name="link_category_key",
		blank=False,
		null=False,
		on_delete=models.CASCADE
	)


class Service(models.Model):
	class Meta():
		db_table = "service"
		verbose_name = "Service"
		verbose_name_plural = "Services"
	
	service_name = models.CharField(
		max_length=100,
		blank=False,
		null=False
	)
	
	service_icon = models.CharField(
		max_length=255,
		blank=True,
		null=True
	)
	
	service_description = models.TextField(
		blank=True,
		null=True
	)


class ServiceText(models.Model):
	class Meta():
		db_table = "servicetext"
		verbose_name = "Service Main Content"
		verbose_name_plural = "Service Main Contents"
	
	service_text = models.TextField(
		blank=False,
		null=False
	)


class Footer(models.Model):
	class Meta():
		db_table = "footer"
		verbose_name = "Footer"
		verbose_name_plural = "Footers"
	
	body = models.CharField(
		max_length=255,
		null=False,
		blank=False
	)


def get_upload_path(self, filename):
	name, ext = os.path.splitext(filename)
	return os.path.join('', str("static/img/") + str(self.slideshow_uuid) + str("/original") + ext)


def get_upload_path_cropped(self, filename):
	name, ext = os.path.splitext(filename)
	return os.path.join('', str("static/img/") + str(self.slideshow_uuid) + str("/cropped") + ext)


def get_upload_path_thumb(self, filename):
	name, ext = os.path.splitext(filename)
	return os.path.join('', str("static/img/") + str(self.slideshow_uuid) + str("/thumb") + ext)


class Slideshow(models.Model):
	class Meta():
		db_table = "slideshow"
		verbose_name = "Slideshow"
		verbose_name_plural = "Slideshows"
	
	slideshow_org_image = models.ImageField(
		upload_to=get_upload_path,
		blank=False,
	)
	
	slideshow_file = models.ImageField(
		upload_to=get_upload_path_cropped,
		blank=True,
	)
	
	slideshow_thumb = models.ImageField(
		upload_to=get_upload_path_thumb,
		blank=True,
	)
	
	slideshow_title = models.CharField(
		max_length=100,
		blank=False,
		null=False
	)
	
	slideshow_body = models.TextField(
		blank=False,
		null=False
	)
	
	slideshow_link = models.URLField(
		blank=False,
		null=False
	)
	
	slideshow_uuid = models.CharField(
		max_length=50,
		default=uuid.uuid4
	)
	
	slideshow_button = models.BooleanField(
		default=1,
		blank=False,
		null=False
	)


class HomepageContent(models.Model):
	class Meta():
		db_table = "homepage_content"
		verbose_name = "Homepage Content"
		verbose_name_plural = "Homepage Contents"
	
	homepage_content_title = models.CharField(
		max_length=100,
		blank=False,
		null=False
	)
	
	homepage_content_body = models.TextField(
		blank=False,
		null=False
	)
	
	homepage_content_link = models.URLField(
		blank=False,
		null=False
	)
	
	homepage_content_image = models.ImageField(
		blank=False,
		null=False
	)


class ContactUs(models.Model):
	class Meta():
		db_table = "contact_us"
		verbose_name = "Contact Us"
		verbose_name_plural = "Contact Us"
		
	contact_us_name = models.CharField(
		max_length=2000,
		blank=True,
		null=True
	)
	
	contact_us_address = models.CharField(
		max_length=2000,
		blank=True,
		null=True
	)

	map_address = models.CharField(
		max_length=500,
		blank=True,
		null=True
	)
	
	
class SocialNetworks(models.Model):
	class Meta():
		db_table = "social_networks"
		verbose_name = "Social"
		verbose_name_plural = "Socials"
		
	social_title = models.CharField(
		max_length=200,
		blank=False,
		null=False
	)
	
	social_link = models.URLField(
		max_length=200,
		blank=False,
		null=False
	)
	
	social_icon = models.CharField(
		max_length=100,
		blank=True,
		null=True
	)


class ProfileImage(models.Model):
	class Meta():
		db_table = "profile_image"
		verbose_name = "Profile Image"
		verbose_name_plural = "Profile Images"
	
	profile_image = models.ImageField(
		blank=False,
		null=False,
		upload_to="static/img/profile/"
	)


class Gdpr(models.Model):
	class Meta():
		db_table = "gdpr"
		verbose_name = "GDPR"
		verbose_name_plural = "GDPR"
		
	gdpr_body = models.TextField(
		blank=False,
		null=False
	)


class HomeBlocks(models.Model):
	class Meta():
		db_table = "home_blocks"
		verbose_name = "Home Block"
		verbose_name_plural = "Home Blocks"
		
	home_block_link = models.CharField(
		max_length=255,
		blank=False,
		null=False
	)
	
	home_block_title = models.CharField(
		max_length=255,
		blank=False,
		null=False
	)
	
	home_block_image = models.ImageField(
		upload_to="static/img/home_blocks/"
	)
