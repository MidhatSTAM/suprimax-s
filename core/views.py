# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseServerError
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect
from django.core.exceptions import ObjectDoesNotExist
from slugify import Slugify
from django.core.paginator import Paginator
from itertools import chain
import re
from decorators import user_login_required

import datetime
import PIL as pil
from PIL import Image

from core.models import Slideshow, AboutUs, Employee, Service, ServiceText, ContactUs, ArticleCategory, Article, \
	SocialNetworks, Link, LinkCategory, ProfileImage, JobOffer, Gdpr, HomeBlocks
from core.forms import SlideshowForm, AboutUsForm, EmployeeForm, ServiceForm, ServiceTextForm, ContactUsForm, \
	ArticleCategoryForm, ArticleForm, SocialForm, LinkCategoryForm, LinkForm, ProfileImageForm, JobOffersForm, GdprForm, \
	HomeBlocksForm

# FOR EMAIL
import smtplib
from smtplib import SMTP_SSL
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
# END FOR EMAIL

# FOR SCREENSHOT
from subprocess import Popen, PIPE
from selenium import webdriver
from sss import settings
import uuid

# END FOR SCREENSHOT


# Create your views here.

args = {}
ROOT = settings.LINK_PATH


def handler404(request):
	if request.path.startswith('/admin/'):
		args['profile_image'] = profile_image = ProfileImage.objects.all()[:1].get()
		return render(request, 'admin_404.html', args, status=404)
	else:
		args['latest_articles_footer'] = Article.objects.filter(
			article_published=1,
			article_category__article_category_published=1
		).order_by('-id')[:2]
		args['address'] = ContactUs.objects.get(id=1)
		args['socials'] = social = SocialNetworks.objects.all().order_by('id')
		return render(request, '404.html', args, status=404)


def homepage(request):
	args['slideshows'] = slideshows = Slideshow.objects.all().order_by('slideshow_title')
	args['latest_articles_footer'] = Article.objects.filter(
		article_published=1,
		article_category__article_category_published=1
	).order_by('-id')[:2]
	args['address'] = ContactUs.objects.get(id=1)
	args['socials'] = social = SocialNetworks.objects.all().order_by('id')
	args['home_blocks'] = HomeBlocks.objects.all()
	return render(request, 'home.html', args)


def about_us(request):
	args['about_us'] = about_us = AboutUs.objects.all()[:1].get()
	args['aboutus_image'] = aboutus_image = AboutUs.objects.values_list('aboutus_image')
	args['employees'] = Employee.objects.all()
	args['latest_articles_footer'] = Article.objects.filter(
		article_published=1,
		article_category__article_category_published=1
	).order_by('-id')[:2]
	args['address'] = ContactUs.objects.get(id=1)
	args['socials'] = social = SocialNetworks.objects.all().order_by('id')
	return render(request, 'company.html', args)


def jobs(request):
	args['job_offers'] = JobOffer.objects.filter(
		job_offer_published=1
	).order_by('id')
	args['latest_articles_footer'] = Article.objects.filter(
		article_published=1,
		article_category__article_category_published=1
	).order_by('-id')[:2]
	args['address'] = ContactUs.objects.get(id=1)
	args['socials'] = social = SocialNetworks.objects.all().order_by('id')
	return render(request, 'job_offers.html', args)


def gdpr(request):
	args['latest_articles_footer'] = Article.objects.filter(
		article_published=1,
		article_category__article_category_published=1
	).order_by('-id')[:2]
	args['address'] = ContactUs.objects.get(id=1)
	args['socials'] = social = SocialNetworks.objects.all().order_by('id')
	args['gdpr'] = Gdpr.objects.all()[:1].get()
	return render(request, 'gdpr.html', args)


def services(request):
	args['service_text'] = service_text = ServiceText.objects.all()[:1].get()
	args['services'] = service = Service.objects.all().order_by('id')
	args['latest_articles_footer'] = Article.objects.filter(
		article_published=1,
		article_category__article_category_published=1
	).order_by('-id')[:2]
	args['address'] = ContactUs.objects.get(id=1)
	args['socials'] = social = SocialNetworks.objects.all().order_by('id')
	return render(request, 'our_services.html', args)


def contact_us(request):
	args['contact_us'] = contact_us = ContactUs.objects.all()[:1].get()
	args['latest_articles_footer'] = Article.objects.filter(
		article_published=1,
		article_category__article_category_published=1
	).order_by('-id')[:2]
	args['address'] = ContactUs.objects.get(id=1)
	args['socials'] = social = SocialNetworks.objects.all().order_by('id')
	
	if request.POST:
		try:
			email_from = "info@suprimax.se"
			email_from_pass = ""
			email_to = 'midhatstam@gmail.com'
			email_cc = ''
			email_bcc = ''
			email_to_addresses = email_to + email_cc + email_bcc
			email_subject = "Email sent from Contact Page"
			email_html_text = '<html><body><p>MESSAGE FROM: ' + u''.join(
				request.POST.get('contact_name')) + '<p>MESSAGE E-MAIL: ' + u''.join(
				request.POST.get('contact_email')) + '</p><p>MESSAGE:<br>' + u''.join(
				request.POST.get('contact_message')) + '</body></html>'
			
			msg = MIMEMultipart()
			msg['Subject'] = email_subject
			msg['From'] = email_from
			msg['To'] = email_to
			msg['CC'] = email_cc
			msg['BCC'] = email_bcc
			msg_html = MIMEText(email_html_text, 'html')
			msg.attach(msg_html)
			
			server = smtplib.SMTP_SSL('mail03.portsit.se', 465)
			# server.login(email_from, email_from_pass)
			
			server.sendmail(email_from, [email_to, email_cc, email_bcc], msg.as_string())
			server.quit()
			msg = "E-mail has been sent"
		except:
			msg = "Error occured while sending e-mail"
			pass
		return redirect("/contact-us/" + "?message=" + str(msg))
	else:
		return render(request, "contact.html", args)


def links(request):
	args['latest_articles_footer'] = Article.objects.filter(
		article_published=1,
		article_category__article_category_published=1
	).order_by('-id')[:2]
	args['address'] = ContactUs.objects.get(id=1)
	args['socials'] = social = SocialNetworks.objects.all().order_by('id')
	args['link_categories'] = LinkCategory.objects.all().order_by('id')
	args['links'] = Link.objects.all().order_by('id')
	return render(request, 'links.html', args)


def portal(request):
	args['address'] = ContactUs.objects.get(id=1)
	args['latest_articles_footer'] = Article.objects.filter(
		article_published=1,
		article_category__article_category_published=1
	).order_by('-id')[:2]
	args['socials'] = social = SocialNetworks.objects.all().order_by('id')
	return render(request, 'portal.html', args)


def news(request, page_number=1):
	args['categories'] = ArticleCategory.objects.filter(
		article_category_published=1
	).order_by('id')
	all_articles = Article.objects.filter(
		article_published=1,
		article_category__article_category_published=1
	).order_by('-id')
	args['latest_articles'] = Article.objects.filter(
		article_published=1,
		article_category__article_category_published=1
	).order_by('-id')[:3]
	args['latest_articles_footer'] = Article.objects.filter(
		article_published=1,
		article_category__article_category_published=1
	).order_by('-id')[:2]
	args['socials'] = social = SocialNetworks.objects.all().order_by('id')
	args['address'] = ContactUs.objects.get(id=1)
	
	articles_page = Paginator(all_articles, 5)
	try:
		args['articles'] = articles_page.page(page_number)
	except:
		args['articles'] = articles_page.page(1)
	return render(request, 'news.html', args)


def news_article_search(request, page_number=1):
	args['article'] = None
	args['address'] = ContactUs.objects.get(id=1)
	args['latest_articles_footer'] = Article.objects.filter(
		article_published=1,
		article_category__article_category_published=1
	).order_by('-id')[:2]
	args['socials'] = social = SocialNetworks.objects.all().order_by('id')
	if request.method == 'POST':
		search_query = request.POST.get('search_form')
		if search_query != "":
			### STARTING SEARCH OF ARTICLES ###
			
			article_title_search = Article.objects.filter(
				article_title__icontains=str(request.POST.get('search_form')),
				article_published=1,
				article_category__article_category_published=1
			).exclude(
				article_title__icontains="",
			)
			
			article_body_search = Article.objects.filter(
				article_body__icontains=str(request.POST.get('search_form')),
				article_published=1,
				article_category__article_category_published=1
			).exclude(
				id__in=article_title_search.values_list('id'),
				article_body__icontains=""
			)
			
			article_metakey_search = Article.objects.filter(
				article_metakey__icontains=str(request.POST.get('search_form')),
				article_published=1,
				article_category__article_category_published=1
			).exclude(
				id__in=article_body_search.values_list('id'),
				article_metakey__icontains=""
			)
			
			article_category_search = Article.objects.filter(
				article_category__article_category_title__icontains=str(request.POST.get('search_form')),
				article_published=1,
				article_category__article_category_published=1
			).exclude(
				id__in=article_metakey_search.values_list('id'),
				article_category__article_category_title__icontains=""
			)
			
			all_articles = list(
				chain(
					article_title_search,
					article_body_search,
					article_metakey_search,
					article_category_search,
				)
			)
			
			page_number = request.POST.get('page_number', "1")
			
			articles_page = Paginator(all_articles, 20)
			try:
				args['search_articles'] = articles_page.page(page_number)
			except:
				args['search_articles'] = articles_page.page(1)
			
			args['latest_articles_footer'] = Article.objects.filter(
				article_published=1,
				article_category__article_category_published=1
			).order_by('-id')[:4]
			
			args['search'] = str(request.POST.get('search_form')).encode("UTF8")
			return render(request, 'blocks/sections/news/news_search.html', args)
		else:
			return HttpResponseRedirect('/news/')
	else:
		return HttpResponseRedirect('/links/')


def get_prev_and_next_items(target, items):
	''' To get previous and next objects from QuerySet '''
	found = False
	prev = None
	next = None
	for item in items:
		if found:
			next = item
			break
		if item.id == target.id:
			found = True
			continue
		prev = item
	return (prev, next)


def news_single_page(request, article_alias):
	args['article_alias'] = alias = article_alias
	args['categories'] = ArticleCategory.objects.filter(
		article_category_published=1
	).order_by('id')
	args['latest_articles'] = Article.objects.filter(
		article_published=1,
		article_category__article_category_published=1
	).order_by('-id')[:3]
	args['latest_articles_footer'] = Article.objects.filter(
		article_published=1,
		article_category__article_category_published=1
	).order_by('-id')[:4]
	args['address'] = ContactUs.objects.get(id=1)
	args['socials'] = social = SocialNetworks.objects.all().order_by('id')
	args['article'] = article = get_object_or_404(
		Article,
		article_published=1,
		article_category__article_category_published=1,
		article_alias=alias
	)
	
	try:
		args['next_article'] = next_article = article.get_next_by_article_edited_date()
	except Article.DoesNotExist:
		args['next_article'] = next_article = None
		
	try:
		args['previous_article'] = previous_article = article.get_previous_by_article_edited_date()
	except Article.DoesNotExist:
		args['previous_article'] = previous_article = None
	
	return render(request, 'news_single_page.html', args)


def user_login(request):
	username = request.POST.get('username', '')
	password = request.POST.get('password', '')
	user = authenticate(username=username, password=password)
	url_redirect = request.GET.get('next', '')
	if len(url_redirect) >= 1:
		url_redirect = request.GET.get('next')
	else:
		url_redirect = "/admin/"
	
	if user is not None:
		# return HttpResponseRedirect(url_redirect)
		# the password verified for the user
		if user.is_active:
			login(request, user)
			return HttpResponseRedirect(url_redirect)
		else:
			return HttpResponseRedirect("/admin/login/")
	else:
		return HttpResponseRedirect("/admin/login/")


@user_login_required
def admin_dashboard(request):
	args['profile_image'] = profile_image = ProfileImage.objects.all()[:1].get()
	return render(request, 'admin_dashboard.html')


@user_login_required
def admin_slideshows_list(request):
	args['slideshows'] = Slideshow.objects.all()
	args['profile_image'] = profile_image = ProfileImage.objects.all()[:1].get()
	try:
		args['gdpr'] = gdpr = Gdpr.objects.all()[:1].get()
	except:
		gdpr = None
	args['home_blocks'] = HomeBlocks.objects.all().order_by('id')
	return render(request, 'admin_edit_homepage.html', args)


@user_login_required
def admin_gdpr(request):
	if request.POST:
		try:
			gdpr = Gdpr.objects.all()[:1].get()
		except:
			gdpr = None
		
		if gdpr is not None:
			gdpr.gdpr_body = request.POST.get('elm1')
			gdpr.save()
			return HttpResponseRedirect('/admin/')
		else:
			gdpr_save_form = GdprForm(request.POST)
			if gdpr_save_form.is_valid():
				gdpr = gdpr_save_form.save(commit=False)
				gdpr.gdpr_body = request.POST.get('elm1')
				gdpr.save()
				return HttpResponseRedirect('/admin/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/admin/' + message + msg_txt)
	else:
		args['gdpr'] = Gdpr.objects.all()[:1].get()
		return render(request, 'admin_edit_homepage.html', args)
	

@user_login_required
def admin_home_blocks_add(request):
	if request.POST:
		try:
			home_block = HomeBlocks.objects.get(
				id__exact=request.POST.get('home_block_id')
			)
		except:
			home_block = None
		
		if home_block is not None:
			home_block.home_block_title = request.POST.get('home_block_title')
			home_block.home_block_link = request.POST.get('home_block_link')
			home_block.home_block_image = request.POST.get('home_block_image')
			home_block.save()
			return HttpResponseRedirect('/admin/')
		else:
			home_block_save_form = HomeBlocksForm(request.POST, request.FILES)
			if home_block_save_form.is_valid():
				home_block = home_block_save_form.save(commit=False)
				home_block.home_block_title = request.POST.get('home_block_title')
				home_block.home_block_link = request.POST.get('home_block_link')
				home_block.home_block_image = request.FILES['home_block_image']
				home_block.save()
				return HttpResponseRedirect('/admin/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/admin/' + message + msg_txt)
	else:
		message = str("?message_type=success&message=")
		args['msg_txt'] = msg_txt = str("Error. Save method need to be POST")
		return HttpResponseRedirect('/admin/' + message + msg_txt)


@user_login_required
def admin_home_blocks_delete(request):
	if request.POST:
		try:
			home_block_id = request.POST.get('home_block_delete')
			home_block = HomeBlocks.objects.get(id=home_block_id)
			home_block.delete()
			return HttpResponseRedirect('/admin/')
		except ObjectDoesNotExist:
			home_block = None
			return HttpResponseRedirect('/admin/')
	else:
		return HttpResponseRedirect('/admin/')


@user_login_required
def admin_slideshow_add(request):
	if request.POST:
		try:
			slideshow = Slideshow.objects.get(
				id__exact=request.POST.get('slideshow_id')
			)
		except:
			slideshow = None
		
		if slideshow is not None:
			slideshow.slideshow_title = request.POST.get('slideshow_title')
			slideshow.slideshow_body = request.POST.get('slideshow_body')
			slideshow.slideshow_link = request.POST.get('slideshow_link')
			slideshow.slideshow_image = request.POST.get('slideshow_image')
			slideshow.save()
			return HttpResponseRedirect('/admin/')
		else:
			slideshow_save_form = SlideshowForm(request.POST, request.FILES)
			if slideshow_save_form.is_valid():
				slideshow = slideshow_save_form.save(commit=False)
				slideshow.slideshow_title = request.POST.get('slideshow_title')
				slideshow.slideshow_body = request.POST.get('slideshow_body')
				slideshow.slideshow_link = request.POST.get('slideshow_link')
				slideshow.slideshow_org_image = request.FILES['slideshow_image']
				slideshow.slideshow_file = request.FILES['slideshow_image']
				slideshow.slideshow_thumb = request.FILES['slideshow_image']
				slideshow.save()
				
				# ----------Open Original Photo--------------
				
				original = Slideshow.objects.get(id=slideshow.id)
				org = Image.open(original.slideshow_org_image)
				icc_profile = org.info.get('icc_profile')
				
				# ------------Cropping image----------------- #
				
				crop_photo = pil.Image.Image.copy(org)
				
				width = crop_photo.size[0]
				height = crop_photo.size[1]
				
				aspect = width / float(height)
				
				ideal_width = 2880
				ideal_height = 1050
				
				ideal_aspect = ideal_width / float(ideal_height)
				
				if aspect > ideal_aspect:
					# Then crop the left and right edges:
					new_width = int(ideal_aspect * height)
					offset = (width - new_width) / 2
					resize = (offset, 0, width - offset, height)
				else:
					# ... crop the top and bottom:
					new_height = int(width / ideal_aspect)
					offset = (height - new_height) / 2
					resize = (0, offset, width, height - offset)
				
				# Cropping prepared photo and saving it as cropped photo
				cropped = crop_photo.crop(resize)
				get_upload_path_cropped = str("static/img/") + str(original.slideshow_uuid) + str("/cropped.png")
				cropped.save(get_upload_path_cropped, format="PNG", icc_profile=icc_profile)
				
				# -------------End of cropping--------------
				
				# -----------Thumbnailing image-------------
				thumbnail = pil.Image.Image.copy(org)
				
				width = thumbnail.size[0]
				height = thumbnail.size[1]
				
				aspect = width / float(height)
				
				ideal_width = 100
				ideal_height = 100
				
				ideal_aspect = ideal_width / float(ideal_height)
				
				if aspect > ideal_aspect:
					# Then crop the left and right edges:
					new_width = int(ideal_aspect * height)
					offset = (width - new_width) / 2
					resize = (offset, 0, width - offset, height)
				else:
					# ... crop the top and bottom:
					new_height = int(width / ideal_aspect)
					offset = (height - new_height) / 2
					resize = (0, offset, width, height - offset)
				
				thumbed = thumbnail.crop(resize).resize((ideal_width, ideal_height), Image.ANTIALIAS)
				get_upload_path_thumb = str("static/img/") + str(original.slideshow_uuid) + str("/thumb.png")
				thumbed.save(get_upload_path_thumb, format="PNG", icc_profile=icc_profile)
				
				return HttpResponseRedirect('/admin/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/admin/' + message + msg_txt)
	else:
		message = str("?message_type=success&message=")
		args['msg_txt'] = msg_txt = str("Error. Save method need to be POST")
		return HttpResponseRedirect('/admin/' + message + msg_txt)


@user_login_required
def admin_slideshow_delete(request):
	if request.POST:
		try:
			slideshow_id = request.POST.get('slideshow_delete')
			slideshow = Slideshow.objects.get(id=slideshow_id)
			slideshow.delete()
			return HttpResponseRedirect('/admin/')
		except ObjectDoesNotExist:
			slideshow = None
			return HttpResponseRedirect('/admin/')
	else:
		return HttpResponseRedirect('/admin/')


@user_login_required
def admin_about_us(request):
	if request.POST:
		try:
			about_us = AboutUs.objects.all()[:1].get()
		except:
			about_us = None
		
		if about_us is not None:
			about_us.aboutus_body = request.POST.get('elm1')
			image = request.POST.get('aboutus_image')
			if image == "":
				pass
			else:
				about_us.aboutus_image = request.FILES['aboutus_image']
			about_us.save()
			return HttpResponseRedirect('/admin/about-us/')
		else:
			about_us_save_form = AboutUsForm(request.POST, request.FILES)
			if about_us_save_form.is_valid():
				about_us = about_us_save_form.save(commit=False)
				about_us.aboutus_body = request.POST.get('elm1')
				if request.FILES['aboutus_image'] == "":
					pass
				else:
					about_us.aboutus_image = request.FILES['aboutus_image']
				about_us.save()
				return HttpResponseRedirect('/admin/about-us')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/admin/about-us/' + message + msg_txt)
	else:
		try:
			args['profile_image'] = profile_image = ProfileImage.objects.all()[:1].get()
			args['about_us'] = about_us = AboutUs.objects.all()[:1].get()
			args['aboutus_image'] = aboutus_image = AboutUs.objects.values_list('aboutus_image')
		
		except:
			about_us = None
			aboutus_image = None
		
		return render(request, 'admin_about_us.html', args)


@user_login_required
def admin_team_list(request):
	args['team_members'] = Employee.objects.all()
	args['profile_image'] = profile_image = ProfileImage.objects.all()[:1].get()
	return render(request, 'admin_team.html', args)


@user_login_required
def admin_team_add(request):
	if request.POST:
		try:
			employee = Employee.objects.get(
				id__exact=request.POST.get('employee_id')
			)
		except:
			employee = None
		
		if employee is not None:
			employee.employee_full_name = request.POST.get('employee_fullname')
			employee.employee_job = request.POST.get('employee_title')
			employee.employee_facebook = request.POST.get('employee_facebook')
			employee.employee_twitter = request.POST.get('employee_twitter')
			employee.employee_email = request.POST.get('employee_email')
			employee.employee_phone = request.POST.get('employee_phone')
			employee.employee_image = request.POST.get('employee_image')
			employee.save()
			return HttpResponseRedirect('/admin/our-team/')
		else:
			employee_save_form = EmployeeForm(request.POST, request.FILES)
			if employee_save_form.is_valid():
				employee = employee_save_form.save(commit=False)
				employee.employee_full_name = request.POST.get('employee_fullname')
				employee.employee_job = request.POST.get('employee_title')
				employee.employee_facebook = request.POST.get('employee_facebook')
				employee.employee_twitter = request.POST.get('employee_twitter')
				employee.employee_email = request.POST.get('employee_email')
				employee.employee_phone = request.POST.get('employee_phone')
				if 'employee_image' in request.FILES:
					employee.employee_image = request.FILES['employee_image']
				employee.save()
				return HttpResponseRedirect('/admin/our-team/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/admin/our-team/' + message + msg_txt)
	else:
		message = str("?message_type=success&message=")
		args['msg_txt'] = msg_txt = str("Error. Save method need to be POST")
		return HttpResponseRedirect('/admin/our-team/' + message + msg_txt)


@user_login_required
def admin_team_delete(request):
	if request.POST:
		try:
			employee_id = request.POST.get('employee_delete')
			employee = Employee.objects.get(id=employee_id)
			employee.delete()
			return HttpResponseRedirect('/admin/our-team/')
		except ObjectDoesNotExist:
			employee = None
			return HttpResponseRedirect('/admin/our-team/')
	else:
		return HttpResponseRedirect('/admin/our-team/')


@user_login_required
def admin_service_text(request):
	if request.POST:
		try:
			service_text = ServiceText.objects.all()[:1].get()
		except:
			service_text = None
		
		if service_text is not None:
			service_text.service_text = request.POST.get('elm1')
			service_text.save()
			return HttpResponseRedirect('/admin/services/')
		else:
			service_text_save_form = ServiceTextForm(request.POST)
			if service_text_save_form.is_valid():
				service_text = service_text_save_form.save(commit=False)
				service_text.service_text = request.POST.get('elm1')
				service_text.save()
				return HttpResponseRedirect('/admin/services/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/admin/services/' + message + msg_txt)
	else:
		try:
			args['profile_image'] = profile_image = ProfileImage.objects.all()[:1].get()
			args['service_text'] = service_text = ServiceText.objects.all()[:1].get()
			args['services'] = services = Service.objects.all().order_by('id')
		
		except:
			service_text = None
		
		return render(request, 'admin_services.html', args)


@user_login_required
def admin_service_edit(request):
	args['service'] = Service.objects.get(id=request.POST.get('service_id'))
	return render(request, "admin/sections/services/services_edit.html", args)


@user_login_required
def admin_service_add(request):
	if request.POST:
		try:
			service = Service.objects.get(
				id__exact=request.POST.get('service_id')
			)
		except:
			service = None
		
		if service is not None:
			service.service_name = request.POST.get('service_name')
			service.service_description = request.POST.get('service_description')
			service.service_icon = request.POST.get('service_icon')
			service.save()
			return HttpResponseRedirect('/admin/services/')
		else:
			service_save_form = ServiceForm(request.POST)
			if service_save_form.is_valid():
				service = service_save_form.save(commit=False)
				service.service_name = request.POST.get('service_name')
				service.service_description = request.POST.get('service_description')
				service.service_icon = request.POST.get('service_icon')
				service.save()
				return HttpResponseRedirect('/admin/services/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/admin/services/' + message + msg_txt)
	else:
		try:
			args['service'] = service = Service.objects.all().order_by('id')
		
		except:
			service = None
		
		return render(request, 'admin_services.html', args)


@user_login_required
def admin_service_delete(request):
	if request.POST:
		try:
			service_id = request.POST.get('service_delete')
			service = Service.objects.get(id=service_id)
			service.delete()
			return HttpResponseRedirect('/admin/services/')
		except ObjectDoesNotExist:
			service = None
			return HttpResponseRedirect('/admin/services/')
	else:
		return HttpResponseRedirect('/admin/services/')


@user_login_required
def admin_service_close(request):
	args['service'] = Service.objects.get(id=request.POST.get('service_id'))
	return render(request, "admin/sections/services/services_close.html", args)


@user_login_required
def admin_contact_us(request):
	if request.POST:
		try:
			contact_us = ContactUs.objects.all()[:1].get()
		except:
			contact_us = None
		
		if contact_us is not None:
			contact_us.contact_us_name = request.POST.get('elm1')
			contact_us.contact_us_address = request.POST.get('elm2')
			contact_us.map_address = request.POST.get('map-address')
			contact_us.save()
			return HttpResponseRedirect('/admin/contact/')
		else:
			contact_us_save_form = ContactUsForm(request.POST)
			if contact_us_save_form.is_valid():
				contact_us = contact_us_save_form.save(commit=False)
				contact_us.contact_us_name = request.POST.get('elm1')
				contact_us.contact_us_address = request.POST.get('elm2')
				contact_us.map_address = request.POST.get('map-address')
				contact_us.save()
				return HttpResponseRedirect('/admin/contact/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/admin/contact/' + message + msg_txt)
	else:
		try:
			args['profile_image'] = profile_image = ProfileImage.objects.all()[:1].get()
			args['contact_us'] = contact_us = ContactUs.objects.all()[:1].get()
		
		except:
			contact_us = None
		
		return render(request, "admin_contact_us.html", args)


@user_login_required
def admin_news_category(request):
	if request.POST:
		try:
			category = ArticleCategory.objects.get(
				id__exact=request.POST.get('category_id')
			)
		except:
			category = None
		
		if category is not None:
			category.article_category_title = request.POST.get('article_category_title')
			category.article_category_alias = request.POST.get('article_category_alias')
			if request.POST.get('article_category_published') == "1":
				category.article_category_published = 1
			else:
				category.article_category_published = 0
			category.save()
			return HttpResponseRedirect('/admin/news/')
		else:
			category_save_form = ArticleCategoryForm(request.POST)
			if category_save_form.is_valid():
				category = category_save_form.save(commit=False)
				category.article_category_title = request.POST.get('article_category_title')
				category.article_category_alias = request.POST.get('article_category_alias')
				if request.POST.get('article_category_published') == '1':
					category.article_category_published = 1
				else:
					category.article_category_published = 0
				category.save()
				return HttpResponseRedirect('/admin/news/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/admin/news/' + message + msg_txt)
	else:
		try:
			args['profile_image'] = profile_image = ProfileImage.objects.all()[:1].get()
			args['categories'] = categories = ArticleCategory.objects.all()
			args['articles'] = articles = Article.objects.all()
		
		except:
			categories = None
			articles = None
		
		return render(request, "admin_news.html", args)


@user_login_required
def category_alias_available(request):
	if request.method == "GET":
		get = request.GET.copy()
		if get.has_key('article_category_alias'):
			alias_str = get['article_category_alias']
			if ArticleCategory.objects.filter(
					article_category_alias=alias_str,
			).count() == 0:
				return HttpResponse(str(alias_str))
			else:
				return HttpResponseServerError(alias_str)
	return HttpResponseServerError("Requires a slug field.")


@user_login_required
def category_slugify_function(request):
	error_msg = u"No GET data sent."
	if request.method == "GET":
		get = request.GET.copy()
		if get.has_key('article_category_title'):
			category_title = get['article_category_title']
			slug = Slugify(to_lower=True, separator='-')
			alias_slugify = slug(category_title)
			return HttpResponse(alias_slugify)
		else:
			error_msg = u"Insufficient GET data (need 'slug' and 'title'!)"
	return HttpResponseServerError(error_msg)


@user_login_required
def admin_news_change_category_state(request):
	if request.POST:
		try:
			category_id = (request.POST.get('category_id'))
			category = ArticleCategory.objects.get(id=category_id)
			if category.article_category_published == 1:
				category.article_category_published = 0
				category.save(update_fields=['article_category_published'])
				return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/admin/news/'), args)
			else:
				category.article_category_published = 1
				category.save(update_fields=['article_category_published'])
				return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/admin/news/'), args)
		except ObjectDoesNotExist:
			category = None
			return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/admin/news/'), args)


@user_login_required
def admin_news_category_delete(request):
	if request.POST:
		try:
			category_id = request.POST.get('category_delete')
			category = ArticleCategory.objects.get(id=category_id)
			category.delete()
			return HttpResponseRedirect('/admin/news/')
		except ObjectDoesNotExist:
			category = None
			return HttpResponseRedirect('/admin/news/')
	else:
		return HttpResponseRedirect('/admin/news/')


@user_login_required
def admin_news_article_add(request):
	args['article_categories'] = ArticleCategory.objects.all().order_by('id')
	try:
		args['current_article'] = Article.objects.get(id=request.GET.get('id'))
	except:
		args['current_article'] = None
	
	return render(request, 'admin/sections/news/add_news.html', args)


@user_login_required
def admin_news_article_save(request):
	if request.POST:
		try:
			article = Article.objects.get(
				id__exact=request.POST.get('article_id')
			)
		except:
			article = None
		
		if article is not None:
			article.article_title = request.POST.get('article_title')
			article.article_alias = request.POST.get('article_alias')
			if request.POST.get('article_published') == '1':
				article.article_published = 1
			else:
				article.article_published = 0
			article.article_body = request.POST.get('elm2')
			article.article_metakey = request.POST.get('article_tags')
			article.article_category_id = request.POST.get('article_category')
			article.article_edited_date = datetime.datetime.now()
			article.article_image = request.FILES.get('article_image')
			article.save()
			return HttpResponseRedirect('/admin/news/')
		else:
			article_save_form = ArticleForm(request.POST, request.FILES)
			if article_save_form.is_valid():
				article = article_save_form.save(commit=False)
				article.article_title = request.POST.get('article_title')
				article.article_alias = request.POST.get('article_alias')
				if request.POST.get('article_published') == '1':
					article.article_published = 1
				else:
					article.article_published = 0
				article.article_body = request.POST.get('elm2')
				article.article_metakey = request.POST.get('article_tags')
				article.article_category_id = request.POST.get('article_category')
				article.article_edited_date = datetime.datetime.now()
				article.article_image = request.FILES.get('article_image')
				article.save()
				return HttpResponseRedirect('/admin/news/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/admin/news/' + message + msg_txt)
	else:
		message = str("?message_type=error&message=")
		args['msg_txt'] = msg_txt = str("Error while saving. Must be POST.")
		return HttpResponseRedirect('/admin/news/' + message + msg_txt)


@user_login_required
def alias_available(request):
	if request.method == "GET":
		get = request.GET.copy()
		if get.has_key('article_alias'):
			alias_str = get['article_alias']
			if Article.objects.filter(
					article_alias=alias_str,
			).count() == 0:
				return HttpResponse(str(alias_str))
			else:
				return HttpResponseServerError(alias_str)
	return HttpResponseServerError("Requires a slug field.")


@user_login_required
def slugify_function(request):
	error_msg = u"No GET data sent."
	if request.method == "GET":
		get = request.GET.copy()
		if get.has_key('title'):
			article_title = get['title']
			slug = Slugify(to_lower=True, separator='-')
			alias_slugify = slug(article_title)
			return HttpResponse(alias_slugify)
		else:
			error_msg = u"Insufficient GET data (need 'slug' and 'title'!)"
	return HttpResponseServerError(error_msg)


@user_login_required
def admin_news_change_article_state(request):
	if request.POST:
		try:
			article_id = (request.POST.get('article_id'))
			article = Article.objects.get(id=article_id)
			if article.article_published == 1:
				article.article_published = 0
				article.save(update_fields=['article_published'])
				return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/admin/news/'), args)
			else:
				article.article_published = 1
				article.save(update_fields=['article_published'])
				return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/admin/news/'), args)
		except ObjectDoesNotExist:
			article = None
			return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/admin/news/'), args)


@user_login_required
def admin_news_article_delete(request):
	if request.POST:
		try:
			article_id = request.POST.get('article_delete')
			article = Article.objects.get(id=article_id)
			article.delete()
			return HttpResponseRedirect('/admin/news/')
		except ObjectDoesNotExist:
			article = None
			return HttpResponseRedirect('/admin/news/')
	else:
		return HttpResponseRedirect('/admin/news/')


@user_login_required
def admin_social(request):
	if request.POST:
		try:
			social = SocialNetworks.objects.get(
				id__exact=request.POST.get('social_id')
			)
		except:
			social = None
		
		if social is not None:
			social.social_title = request.POST.get('social_name')
			social.social_link = request.POST.get('social_link')
			social.social_icon = request.POST.get('social_icon')
			social.save()
			return HttpResponseRedirect('/admin/social/')
		else:
			social_save_form = SocialForm(request.POST)
			if social_save_form.is_valid():
				social = social_save_form.save(commit=False)
				social.social_title = request.POST.get('social_name')
				social.social_link = request.POST.get('social_link')
				social.social_icon = request.POST.get('social_icon')
				social.save()
				
				try:
					social = SocialNetworks.objects.filter(id=social.id)
				except:
					social = None
				if social is not None:
					link_save_form = LinkForm(request.POST)
					if link_save_form.is_valid():
						link = link_save_form.save(commit=False)
						link.link_name = social.social_title
						link.link_url = social.social_link
						link.link_category_id = "2"
						link_url = social.social_link
						if re.match(
								"(?:https?://)?(?:www)?(?:[\w-]{2,255}(?:\.))?((?:facebook){1,2})(?:/[\w&%?#-]{1,300})?",
								link_url
						):
							link.link_image = "static/img/link/social/facebook.png"
						elif re.match(
								"(?:https?://)?(?:www)?(?:[\w-]{2,255}(?:\.))?((?:twitter){1,2})(?:/[\w&%?#-]{1,300})?",
								link_url
						):
							link.link_image = "static/img/link/social/twitter.png"
						elif re.match(
								"(?:https?://)?(?:www)?(?:[\w-]{2,255}(?:\.))?((?:instagram){1,2})(?:/[\w&%?#-]{1,300})?",
								link_url
						):
							link.link_image = "static/img/link/social/instagram.png"
						elif re.match(
								"(?:https?://)?(?:www)?(?:[\w-]{2,255}(?:\.))?((?:linkedin){1,2})(?:/[\w&%?#-]{1,300})?",
								link_url
						):
							link.link_image = "static/img/link/social/linkedin.png"
						elif re.match(
								"(?:https?://)?(?:www)?(?:[\w-]{2,255}(?:\.))?((?:pinterest){1,2})(?:/[\w&%?#-]{1,300})?",
								link_url
						):
							link.link_image = "static/img/link/social/pinterest.png"
						elif re.match(
								"(?:https?://)?(?:www)?(?:[\w-]{2,255}(?:\.))?((?:skype){1,2})(?:/[\w&%?#-]{1,300})?",
								link_url
						):
							link.link_image = "static/img/link/social/skype.png"
						elif re.match(
								"(?:https?://)?(?:www)?(?:[\w-]{2,255}(?:\.))?((?:whatsapp){1,2})(?:/[\w&%?#-]{1,300})?",
								link_url
						) or re.match("(\d+)", link_url):
							link.link_image = "static/img/link/social/whatsapp.png"
						elif re.match(
								"(?:https?://)?(?:www)?(?:[\w-]{2,255}(?:\.))?((?:google){1,2})(?:/[\w&%?#-]{1,300})?",
								link_url
						):
							link.link_image = "static/img/link/social/googleplus.png"
						link.save()
					else:
						pass
				else:
					pass
				return HttpResponseRedirect('/admin/social/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/admin/social/' + message + msg_txt)
	else:
		try:
			args['profile_image'] = profile_image = ProfileImage.objects.all()[:1].get()
			args['socials'] = social = SocialNetworks.objects.all().order_by('id')
		
		except:
			social = None
		
		return render(request, 'admin_social_networks.html', args)


@user_login_required
def admin_social_edit(request):
	args['social'] = SocialNetworks.objects.get(id=request.POST.get('social_id'))
	return render(request, "admin/sections/social/social_edit.html", args)


@user_login_required
def admin_social_delete(request):
	if request.POST:
		try:
			social_id = request.POST.get('social_delete')
			social = SocialNetworks.objects.get(id=social_id)
			try:
				link = Link.objects.filter(
					link_name__iexact=social.social_title
				)
			except:
				link = None
			if link is not None:
				link.delete()
			social.delete()
			return HttpResponseRedirect('/admin/social/')
		except ObjectDoesNotExist:
			social = None
			link = None
			return HttpResponseRedirect('/admin/social/')
	else:
		return HttpResponseRedirect('/admin/social/')


@user_login_required
def admin_social_close(request):
	args['social'] = SocialNetworks.objects.get(id=request.POST.get('social_id'))
	return render(request, "admin/sections/social/social_close.html", args)


@user_login_required
def admin_links_category(request):
	if request.POST:
		try:
			category = LinkCategory.objects.get(
				id__exact=request.POST.get('category_id')
			)
		except:
			category = None
		
		if category is not None:
			category.link_category_name = request.POST.get('link_category_title')
			category.link_category_alias = request.POST.get('link_category_alias')
			category.save()
			return HttpResponseRedirect('/admin/links/')
		else:
			category_save_form = LinkCategoryForm(request.POST)
			if category_save_form.is_valid():
				category = category_save_form.save(commit=False)
				category.link_category_name = request.POST.get('link_category_title')
				category.link_category_alias = request.POST.get('link_category_alias')
				category.save()
				return HttpResponseRedirect('/admin/links/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/admin/links/' + message + msg_txt)
	else:
		try:
			args['profile_image'] = profile_image = ProfileImage.objects.all()[:1].get()
			args['link_categories'] = categories = LinkCategory.objects.all()
			args['links'] = links = Link.objects.all()
		
		except:
			categories = None
			links = None
		
		return render(request, "admin_links.html", args)


@user_login_required
def link_category_alias_available(request):
	if request.method == "GET":
		get = request.GET.copy()
		if get.has_key('link_category_alias'):
			alias_str = get['link_category_alias']
			if LinkCategory.objects.filter(
					link_category_alias=alias_str,
			).count() == 0:
				return HttpResponse(str(alias_str))
			else:
				return HttpResponseServerError(alias_str)
	return HttpResponseServerError("Requires a slug field.")


@user_login_required
def link_category_slugify_function(request):
	error_msg = u"No GET data sent."
	if request.method == "GET":
		get = request.GET.copy()
		if get.has_key('link_category_title'):
			category_title = get['link_category_title']
			slug = Slugify(to_lower=True, separator='-')
			alias_slugify = slug(category_title)
			return HttpResponse(alias_slugify)
		else:
			error_msg = u"Insufficient GET data (need 'slug' and 'title'!)"
	return HttpResponseServerError(error_msg)


@user_login_required
def admin_links_category_delete(request):
	if request.POST:
		try:
			category_id = request.POST.get('category_delete')
			category = LinkCategory.objects.get(id=category_id)
			category.delete()
			return HttpResponseRedirect('/admin/links/')
		except ObjectDoesNotExist:
			category = None
			return HttpResponseRedirect('/admin/links/')
	else:
		return HttpResponseRedirect('/admin/links/')


def admin_links_add(request):
	args['link_categories'] = LinkCategory.objects.all().order_by('id')
	args['profile_image'] = profile_image = ProfileImage.objects.all()[:1].get()
	try:
		args['current_link'] = Link.objects.get(id=request.GET.get('id'))
	except:
		args['current_link'] = None
	
	return render(request, 'admin/sections/links/add_link.html', args)


def execute_command(command):
	result = Popen(command, shell=True, stdout=PIPE).stdout.read()


# if len(result) > 0 and not result.isspace():
# 	raise Exception(result)


def do_screen_capturing(url, screen_path, width, height):
	print "Capturing screen.."
	driver = webdriver.PhantomJS('/home/suprimax/phantomjs')
	# it save service log file in same directory
	# if you want to have log file stored else where
	# initialize the webdriver.PhantomJS() as
	# driver = webdriver.PhantomJS(service_log_path='/var/log/phantomjs/ghostdriver.log')
	driver.set_script_timeout(50)
	if width and height:
		driver.set_window_size(width, height)
	driver.get(url)
	driver.save_screenshot(screen_path)


def do_crop(params):
	print "Croping captured image.."
	command = [
		'convert',
		params['screen_path'],
		'-crop', '%sx%s+0+0' % (params['width'], params['height']),
		params['crop_path']
	]
	execute_command(' '.join(command))


def do_thumbnail(params):
	print "Generating thumbnail from croped captured image.."
	command = [
		'convert',
		params['crop_path'],
		'-filter', 'Lanczos',
		'-thumbnail', '%sx%s' % (params['width'], params['height']),
		params['thumbnail_path']
	]
	execute_command(' '.join(command))


def get_screen_shot(**kwargs):
	url = kwargs['url']
	width = int(kwargs.get('width', 1024))  # screen width to capture
	height = int(kwargs.get('height', 1024))  # screen height to capture
	filename = kwargs.get('filename', 'screen.png')  # file name e.g. screen.png
	path = str(str(ROOT) + str(uuid.uuid4()))  # directory path to store screen
	
	crop = kwargs.get('crop', False)  # crop the captured screen
	crop_width = int(kwargs.get('crop_width', width))  # the width of crop screen
	crop_height = int(kwargs.get('crop_height', height))  # the height of crop screen
	crop_replace = kwargs.get('crop_replace', False)  # does crop image replace original screen capture?
	
	thumbnail = kwargs.get('thumbnail', False)  # generate thumbnail from screen, requires crop=True
	thumbnail_width = int(kwargs.get('thumbnail_width', width))  # the width of thumbnail
	thumbnail_height = int(kwargs.get('thumbnail_height', height))  # the height of thumbnail
	thumbnail_replace = kwargs.get('thumbnail_replace', False)  # does thumbnail image replace crop image?
	
	screen_path = str(path) + str(filename)
	crop_path = thumbnail_path = screen_path
	
	if thumbnail and not crop:
		raise Exception, 'Thumnail generation requires crop image, set crop=True'
	
	do_screen_capturing(url, screen_path, width, height)
	
	if crop:
		if not crop_replace:
			crop_path = str(path) + '_crop.png'
		params = {
			'width': crop_width, 'height': crop_height,
			'crop_path': crop_path, 'screen_path': screen_path}
		do_crop(params)
		
		if thumbnail:
			if not thumbnail_replace:
				thumbnail_path = str(path) + '_thumbnail.png'
			params = {
				'width': thumbnail_width, 'height': thumbnail_height,
				'thumbnail_path': thumbnail_path, 'crop_path': crop_path}
			do_thumbnail(params)
	return screen_path, crop_path, thumbnail_path


def admin_links_save(request):
	if request.POST:
		try:
			link = Link.objects.get(
				id__exact=request.POST.get('link_id')
			)
		except:
			link = None
		
		if link is not None:
			link.link_name = request.POST.get('link_title')
			link.link_url = request.POST.get('link_url')
			link.link_category_id = request.POST.get('link_category')
			link.link_image = request.FILES.get('link_image')
			link.save()
			return HttpResponseRedirect('/admin/links/')
		else:
			link_save_form = LinkForm(request.POST, request.FILES)
			if link_save_form.is_valid():
				link = link_save_form.save(commit=False)
				link.link_name = request.POST.get('link_title')
				link.link_url = request.POST.get('link_url')
				link.link_category_id = request.POST.get('link_category')
				url = request.POST.get('link_url')
				if request.POST.get('link_category') == "1":
					screen_path, crop_path, thumbnail_path = get_screen_shot(
						url=url, filename='_original.png',
						crop=True, crop_replace=False,
						thumbnail=True, thumbnail_replace=False,
						thumbnail_width=200, thumbnail_height=150,
					)
					link.link_image = crop_path
				elif request.POST.get('link_category') == "2":
					link_url = request.POST.get('link_url')
					if re.match(
							"(?:https?://)?(?:www)?(?:[\w-]{2,255}(?:\.))?((?:facebook){1,2})(?:/[\w&%?#-]{1,300})?",
							link_url
					):
						link.link_image = "static/img/link/social/facebook.png"
					elif re.match(
							"(?:https?://)?(?:www)?(?:[\w-]{2,255}(?:\.))?((?:twitter){1,2})(?:/[\w&%?#-]{1,300})?",
							link_url
					):
						link.link_image = "static/img/link/social/twitter.png"
					elif re.match(
							"(?:https?://)?(?:www)?(?:[\w-]{2,255}(?:\.))?((?:instagram){1,2})(?:/[\w&%?#-]{1,300})?",
							link_url
					):
						link.link_image = "static/img/link/social/instagram.png"
					elif re.match(
							"(?:https?://)?(?:www)?(?:[\w-]{2,255}(?:\.))?((?:linkedin){1,2})(?:/[\w&%?#-]{1,300})?",
							link_url
					):
						link.link_image = "static/img/link/social/linkedin.png"
					elif re.match(
							"(?:https?://)?(?:www)?(?:[\w-]{2,255}(?:\.))?((?:pinterest){1,2})(?:/[\w&%?#-]{1,300})?",
							link_url
					):
						link.link_image = "static/img/link/social/pinterest.png"
					elif re.match(
							"(?:https?://)?(?:www)?(?:[\w-]{2,255}(?:\.))?((?:skype){1,2})(?:/[\w&%?#-]{1,300})?",
							link_url
					):
						link.link_image = "static/img/link/social/skype.png"
					elif re.match(
							"(?:https?://)?(?:www)?(?:[\w-]{2,255}(?:\.))?((?:whatsapp){1,2})(?:/[\w&%?#-]{1,300})?",
							link_url
					) or re.match("(\d+)", link_url):
						link.link_image = "static/img/link/social/whatsapp.png"
					elif re.match(
							"(?:https?://)?(?:www)?(?:[\w-]{2,255}(?:\.))?((?:google){1,2})(?:/[\w&%?#-]{1,300})?",
							link_url
					):
						link.link_image = "static/img/link/social/googleplus.png"
				elif request.POST.get('link_category') == "3":
					link_url = request.POST.get('link_url')
					if link_url.rsplit('.', 1)[1] == 'doc' or link_url.rsplit('.', 1)[1] == 'docx':
						link.link_image = "static/img/link/downloads/word.png"
					elif link_url.rsplit('.', 1)[1] == 'xls' or link_url.rsplit('.', 1)[1] == 'xlsx':
						link.link_image = "static/img/link/downloads/excel.png"
					elif link_url.rsplit('.', 1)[1] == 'ppt' or link_url.rsplit('.', 1)[1] == 'pptx':
						link.link_image = "static/img/link/downloads/powerpoint.png"
					elif re.match(
							"(?:https?://)?(?:www)?(?:(?:drive)(?:\.))?((?:google){1,2})(?:/[\w&%?#-]{1,300})??",
							link_url
					):
						link.link_image = "static/img/link/downloads/drive.png"
				
				link.save()
				return HttpResponseRedirect('/admin/links/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/admin/links/' + message + msg_txt)
	else:
		message = str("?message_type=error&message=")
		args['msg_txt'] = msg_txt = str("Error while saving. Must be POST.")
		return HttpResponseRedirect('/admin/links/' + message + msg_txt)


@user_login_required
def admin_links_delete(request):
	if request.POST:
		try:
			link_id = request.POST.get('link_delete')
			link = Link.objects.get(id=link_id)
			try:
				social = SocialNetworks.objects.filter(
					social_title__iexact=link.link_name
				)
			except:
				social = None
			if social is not None:
				social.delete()
			link.delete()
			return HttpResponseRedirect('/admin/links/')
		except ObjectDoesNotExist:
			link = None
			social = None
			return HttpResponseRedirect('/admin/links/')
	else:
		return HttpResponseRedirect('/admin/links/')


@user_login_required
def admin_profile_upload(request):
	if request.POST:
		try:
			image = ProfileImage.objects.all()[:1].get()
		except:
			image = None
		
		if image is not None:
			image.profile_image = request.FILES['profile_image']
			image.save()
			return HttpResponseRedirect('/admin/')
		else:
			profile_image_save_form = ProfileImageForm(request.FILES)
			if profile_image_save_form.is_valid():
				image = profile_image_save_form.save(commit=False)
				image.profile_image = request.FILES['profile_image']
				image.save()
				return HttpResponseRedirect('/admin/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/admin/' + message + msg_txt)
	else:
		try:
			args['profile_image'] = profile_image = ProfileImage.objects.all()[:1].get()
		
		except:
			profile_image = None
		
		return render(request, 'admin_dashboard.html', args)


@user_login_required
def admin_job_offers(request):
	args['profile_image'] = profile_image = ProfileImage.objects.all()[:1].get()
	args['job_offers'] = JobOffer.objects.all().order_by('id')
	
	return render(request, 'admin_job_offers.html', args)


@user_login_required
def admin_job_offers_add(request):
	args['profile_image'] = profile_image = ProfileImage.objects.all()[:1].get()
	try:
		args['current_job_offer'] = JobOffer.objects.get(id=request.GET.get('id'))
	except:
		args['current_job_offer'] = None
	
	return render(request, 'admin/sections/job_offers/add_job_offer.html', args)


@user_login_required
def admin_job_offers_save(request):
	if request.POST:
		try:
			job_offer = JobOffer.objects.get(
				id__exact=request.POST.get('job_offer_id')
			)
		except:
			job_offer = None
		
		if job_offer is not None:
			job_offer.job_offer_title = request.POST.get('job_offer_title')
			job_offer.job_offer_description = request.POST.get('job_offer_description')
			job_offer.job_offer_expiring_date = request.POST.get('job_offer_expiring_date')
			if request.POST.get('job_offer_publish') == '1':
				job_offer.job_offer_published = 1
			else:
				job_offer.job_offer_published = 0
			job_offer.save()
			return HttpResponseRedirect('/admin/jobs/')
		else:
			job_offer_save_form = JobOffersForm(request.POST)
			if job_offer_save_form.is_valid():
				job_offer = job_offer_save_form.save(commit=False)
				job_offer.job_offer_title = request.POST.get('job_offer_title')
				job_offer.job_offer_description = request.POST.get('elm1')
				r = request.POST.get('job_offer_expiring_date')
				d = datetime.datetime.strptime(r, '%d.%m.%Y')
				fff = d.strftime('%Y-%m-%d')
				job_offer.job_offer_expiring_date = fff
				if request.POST.get('job_offer_publish') == '1':
					job_offer.job_offer_published = 1
				else:
					job_offer.job_offer_published = 0
				job_offer.save()
				return HttpResponseRedirect('/admin/jobs/')
			else:
				message = str("?message_type=error&message=")
				args['msg_txt'] = msg_txt = str("Error while saving. Form is not valid")
				return HttpResponseRedirect('/admin/jobs/' + message + msg_txt)
	else:
		message = str("?message_type=error&message=")
		args['msg_txt'] = msg_txt = str("Error while saving. Must be POST.")
		return HttpResponseRedirect('/admin/jobs/' + message + msg_txt)


@user_login_required
def admin_change_job_offer_state(request):
	if request.POST:
		try:
			job_offer_id = (request.POST.get('job_offer_id'))
			job_offer = JobOffer.objects.get(id=job_offer_id)
			if job_offer.job_offer_published == 1:
				job_offer.job_offer_published = 0
				job_offer.save(update_fields=['job_offer_published'])
				return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/admin/jobs/'), args)
			else:
				job_offer.job_offer_published = 1
				job_offer.save(update_fields=['job_offer_published'])
				return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/admin/jobs/'), args)
		except ObjectDoesNotExist:
			job_offer = None
			return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/admin/jobs/'), args)


@user_login_required
def admin_job_offer_delete(request):
	if request.POST:
		try:
			job_offer_id = request.POST.get('job_offer_delete')
			job_offer = JobOffer.objects.get(id=job_offer_id)
			job_offer.delete()
			return HttpResponseRedirect('/admin/jobs/')
		except ObjectDoesNotExist:
			job_offer = None
			return HttpResponseRedirect('/admin/jobs/')
	else:
		return HttpResponseRedirect('/admin/jobs/')
