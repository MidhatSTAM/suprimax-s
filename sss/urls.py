"""sss URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^news/', include('news.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls import handler404, handler500
from django.contrib.auth import views as auth_views

from core import views as core_views

urlpatterns = [
	url(r'^django-admin/', admin.site.urls),
	
	# url(r'^admin/$', core_views.admin_dashboard, name='admin_home'),
	url(r'^admin/$', core_views.admin_slideshows_list, name='admin_edit_homepage'),
	
	url(r'^admin/slideshow-add/$', core_views.admin_slideshow_add),
	url(r'^admin/slideshow-delete/$', core_views.admin_slideshow_delete),
	url(r'^admin/gdpr/$', core_views.admin_gdpr),
	url(r'^admin/block/add/$', core_views.admin_home_blocks_add),
	url(r'^admin/block/delete/$', core_views.admin_home_blocks_delete),
	
	url(r'^admin/about-us/$', core_views.admin_about_us, name='admin_about_us'),
	url(r'^admin/our-team/$', core_views.admin_team_list, name='admin_team'),
	url(r'^admin/our-team/add/$', core_views.admin_team_add, name='admin_team_add'),
	url(r'^admin/our-team/delete/$', core_views.admin_team_delete),
	
	url(r'^admin/services/$', core_views.admin_service_text, name='admin_services'),
	url(r'^admin/services/add/$', core_views.admin_service_add),
	url(r'^admin/services/edit/$', core_views.admin_service_edit),
	url(r'^admin/services/close/$', core_views.admin_service_close),
	url(r'^admin/service-delete/$', core_views.admin_service_delete),
	
	url(r'^admin/contact/$', core_views.admin_contact_us, name="admin_contact"),
	
	url(r'^admin/news/$', core_views.admin_news_category, name="admin_news"),
	url(r'^admin/news/category-add/$', core_views.admin_news_category),
	url(r'^admin/news/category-delete/$', core_views.admin_news_category_delete),
	url(r'^admin/news/category/change-category-state/$', core_views.admin_news_change_category_state),
	url(r'^admin/news/article/add/$', core_views.admin_news_article_add, name='add_news'),
	url(r'^admin/news/article/save/$', core_views.admin_news_article_save),
	url(r'^admin/news/article/delete/$', core_views.admin_news_article_delete),
	url(r'^admin/news/article/change-article-state/$', core_views.admin_news_change_article_state),
	url(r'^alias-available/$', core_views.alias_available),
	url(r'^alias-slugify/$', core_views.slugify_function),
	url(r'^category-alias-available/$', core_views.category_alias_available),
	url(r'^category-alias-slugify/$', core_views.category_slugify_function),
	
	url(r'^admin/social/$', core_views.admin_social, name="admin_social"),
	url(r'^admin/social/add/$', core_views.admin_social),
	url(r'^admin/social/edit/$', core_views.admin_social_edit),
	url(r'^admin/social/close/$', core_views.admin_social_close),
	url(r'^admin/social-delete/$', core_views.admin_social_delete),
	
	# url(r'^admin/links/$', core_views.admin_links_category, name="admin_links"),
	# url(r'^admin/links/category-add/$', core_views.admin_links_category),
	# url(r'^admin/links/category-delete/$', core_views.admin_links_category_delete),
	# url(r'^admin/links/add/$', core_views.admin_links_add, name='add_link'),
	# url(r'^admin/links/save/$', core_views.admin_links_save),
	# url(r'^admin/links/delete/$', core_views.admin_links_delete),
	# url(r'^link-category-alias-available/$', core_views.link_category_alias_available),
	# url(r'^link-category-alias-slugify/$', core_views.link_category_slugify_function),
	
	url(r'^admin/jobs/$', core_views.admin_job_offers, name="admin_job_offers"),
	url(r'^admin/jobs/add/$', core_views.admin_job_offers_add, name='add_job_offer'),
	url(r'^admin/jobs/save/$', core_views.admin_job_offers_save),
	url(r'^admin/jobs/delete/$', core_views.admin_job_offer_delete),
	url(r'^admin/jobs/change-job-offer-state/$', core_views.admin_change_job_offer_state),
	
	url(r'^about-us/$', core_views.about_us, name='about_us'),
	url(r'^job-offers/$', core_views.jobs, name='jobs'),
	url(r'^gdpr/$', core_views.gdpr, name='gdpr'),
	url(r'^services/$', core_views.services, name='services'),
	url(r'^contact-us/$', core_views.contact_us, name='contact_us'),
	# url(r'^links/$', core_views.links, name='links'),
	url(r'^portal/$', core_views.portal, name='portal'),
	
	url(r'^news/$', core_views.news, name='news'),
	url(r'^news/(?P<article_alias>[\w\-]+)/$', core_views.news_single_page, name='news_single'),
	url(r'^news/search/$', core_views.news_article_search),
	# url(r'^news/(\d+)/$', core_views.news),
	
	url(r'^admin/login/$', auth_views.login, name='login'),
	url(r'^admin/login/submit/$', core_views.user_login),
	url(r'^admin/logout/$', auth_views.logout, {'next_page': '/admin/'}, name='logout'),
	
	url(r'^$', core_views.homepage, name='homepage'),
	
	url(r'^imagefit/', include('imagefit.urls')),
	
	url(r'^admin/profile/image/$', core_views.admin_profile_upload),
]

handler404 = core_views.handler404
handler500 = core_views.handler404
